# CICD Django

```text
Requirements

   Basic knowledge of Git is recommended but not required
   Familiarity with Linux command lines is useful but not required
   Familiarity with Django framework is recommended
```

## Session 1

1. ### Course content

- Introduction

2. ### Set up Django applications on Dev machines

- Install Python on Windows
- Create and configure a Python virtual environment
- Create a new Django project
- Polling app setup and demo
- An environment config file
- Configure the environment variables
- Commands used :
>- ```python -m venv <env-name>```
>- ```.\<env-name>\Scripts\activate```
>- ```pip install Django```
>- ```django-admin startproject <project-name>```
>- ```pip install -r .\requirements.txt```
>- ```python manage.py migrate```
>- ```python manage.py createsuperuser```
>- ```python manage.py runserver```

3. ### Git and GitLab setup

- GitLab overview
- Create a remote repository on gitlab.com
- Turn your code into a Git project
- Add SSH key to your GitLab account and push your code onto it
- Commands used :
>- ```git init```
>- ```git add .```
>- ```git commit -m "<commit-description>```
>- ```ssh-keygen```
>- ```cat <key-name>.pub```
>- ```git remote add origin <ssh-uri-from-remote>```
>- ```git push origin master```

4. ### Deploying Django to production

- Create and configure Virtual Ubuntu Server (AWS)
- Create and add our own ssh key to server
- SSH into your virtual machines via windows command line terminals
- Create your production sever
- Set up a Dedicated ```deploy``` User on Production Server
- Freezing a Requirements.txt file
- ```Psycopg2``` and ```Gunicorn``` dependencies, needed for server
- Storing static objects
- Add deploy user SSH key to your GitLab account
- Install necessary programs on the production server
- PostgreSQL database setup
- Set up a virtual environment on the prod server
- Get the project code onto the prod server
- Configure the app's environment variables on the prod server
- Serve the application with ```Gunicorn```
- Configure the ```Gunicorn``` services on the prod server
- Set up ```Nginx``` webserver on the prod server
- Commands used :
>- ```ls```
>- ```ls -al```
>- ```ll```
>- ```cd ~```
>- ```cd <dir-name>```
>- ```sudo apt update```
>- ```sudo apt upgrade -y```
>- ```sudo apt install python3-pip```
>- ```sudo apt install python3-venv```
>- ```sudo apt install postgresql postgresql-contrib nginx```
>- ```sudo su```
>- ```adduser <deploy-user-name>```
>- ```usermod -aG sudo <deploy-user-name>```
>- ```source <venv-path>/bin/activate```
>- ```sudo hostnamectl set-hostname <new-hostname>```
>- ```python manage.py collectstatic```
>- ```...and commands from ```[+ref](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-22-04#creating-systemd-socket-and-service-files-for-gunicorn)

## Session 2

5. ### Manual redeployment

- Redeploy your application manually
- Add a ```deploy script``` to the code
- Deploying with the ```deploy script```
- Run sudo commands without passwords
- Commands used :
>- ```sudo visudo```
>- ```%sudo ALL=(ALL:ALL) ALL --> %sudo ALL=(ALL) NOPASSWD: ALL```
>- ```chmod +x <path-to-.sh>```
>- ```git pull```
>- ```python manage.py collectstatic --no-input```
>- ```sudo systemctl reload nginx```
>- ```sudo systemctl restart gunicorn```

6. ### Getting started with Jenkins

- Installing Jenkins
- Expose local Jenkins instance to the internet
- Installing and uninstalling plugins
- Configure Jenkins to send email notifications

7. ### CIC Deployment with GitLab and Jenkins

- Install Git and GitLab plugins on Jenkins
- Configure GitLab-Jenkins connections
- Add Jenkinsfile to your code
- Create a Jenkins pipeline for your project
- Run the pipeline manually
- Configure Jenkinsfile for building and testing
- Configure the deploy stage
- Jenkins SSH into the prod server without a password
- Configure a webhook to trigger the pipeline
- CICD
- Configure Jenkins to send emails when the pipeline fails
- Test the pipeline with failing automated testing

8. ### CIC Delivery with GitLab and Jenkins

- Set up a staging server
- Install necessary programs and set up PSQL database
- Get the project code onto your server
- Configure the app on the server
- Configure Gunicorn service on the server
- Configure Nginx on the server
- Jenkins should deploy to staging first
- Configure Jenkins to request human input before deploying to prod
- Demo: Continuous Integration Continuous Delivery
